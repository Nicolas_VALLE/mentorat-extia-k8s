# Pods simples, multi-conteneurs, pods statiques

---

## Pods

Un **pod** [^5] est un l'objet le plus petit de kubernetes, c'est cet élément qui contient les conteneurs. On retrouve dans un pod les mêmes éléments d'isolation que ceux présents dans un conteneur : **cgroups**, **namaspaces**, etc.

Dans la majorité des cas, **un pod est égal à un conteneur** sauf cas particulier précisé plus bas.
Il est possible d'imaginer une application web composée d'un front, d'une api et d'une base de donnée. Chaqu'un des éléments de l'application sera conteneurisés et contenus dans un pod. Il est possible de schématiser l'ensemble comme suivant :

![schema d'un pod](../Attachments/pod.png)

Un pod ne peut pas être réattribué à un autre [worker](../2.%20Architecture/Composants%20du%20cluster%20K8S%20-%20Control%20plane%20et%20Worker.md). Dans le meilleur des cas, si le pod est lié à un type de **déploiement**, il peut être recréé en cas de panne. Il est important de noter que, comme un conteneur, un pod est **éphémère** et **n'a pas stockage persistant**.

### Cycle de vie d'un pod

La vie d'un pod peut être résumé par le schéma suivant :

![cycle de vie d'un conteneur](../Attachments/cycle%20de%20vie%20pod.png)
**Pending** : La création du pod a été pris en compte par le cluster. Le pod est en cours de téléchargement, en cours d'attribution à un [worker](../2.%20Architecture/Composants%20du%20cluster%20K8S%20-%20Control%20plane%20et%20Worker.md) ou en cours de démarrage. Le pod peut rester en état **pending** si aucun nœud ne peut faire fonctionner le pod suite à une pénurie de ressources ou une contrainte de configuration ( voir les parties [5.5 - Limit et Request](5.5%20-%20Limit%20et%20Request.md) et [5.9 - NodeSelector et NodeName](5.9%20-%20NodeSelector%20et%20NodeName.md))
**Running**: Tous les conteneurs du pod sont créés et un ou plusieurs conteneurs sont toujours actifs.
**Succeeded**: Tous les conteneurs se sont arrêtés sans erreurs.
**Failed**: Tous les conteneurs sont à l'arrêt mais un ou plusieurs d'entre eux se sont arrêtés en erreur.
**Unknown**: L'état du pod n'a pas pu être récupéré. L'erreur survient souvent lors d'une mauvaise communication avec le  [worker](../2.%20Architecture/Composants%20du%20cluster%20K8S%20-%20Control%20plane%20et%20Worker.md) faisant fonctionner le pod.
**CrashLoopBackOff**: Le pod redémarre en boucle. Cet état peut être causé par un conteneur qui crashe fréquemment ou qui ne peut pas démarrer correctement.

Chaque **phase** [^6] de vie est indiqué dans kubernetes grâce à la commande `kubectl get pod`.

### Exemple de création d'un pod

Le manifest d'un pod est simple :

* La partie **`metadata`** permet de lui attribuer un nom mais également des labels
* La partie **`spec`** permet de décrire le conteneur composant le pod

```yml
apiVersion: v1
kind: Pod
metadata:
  name: pod-statique
spec:
  containers:
  - name: nginx
    image: nignx:latest
```

Une multitude d'autre options existe afin de [limiter ou garantir des ressources](Limit%20et%20Request.md), [paramétrer une application](ConfigMap%20et%20Secret.md) ou encore [vérifier que l'application fonctionne correctement](Probes.md). Les options principales sont expliquées dans ce document, d'autre sont présentes [dans le documentation de Kubernetes](https://kubernetes.io/docs/home/).

## Multi-conteneurs

Un pod peut contenir plusieurs conteneurs, dans ce cas, il est appelé **un pod multi-conteneurs** [^1] ou, en anglais, **multi-container pod**.
Toutes les ressources au sein d'un pod sont partagées aux différents conteneurs le composant, ainsi les conteneurs peuvent interagir plus facilement entre eux afin de fournir certaines fonctionnalités.

Il est **important** de noter qu'une bonne pratique consiste à séparer les ressources dans différents pod, mais dans certains cas, il est nécessaire de procéder ainsi (proxy-sidecar, log-sidecar).

Les **multi-conteneurs** ne sont pas à mélanger avec les **[init conteneurs](5.6%20-%20Init-containers.md)**.

### Les interactions

Les ressources partagées sont les suivantes :

#### Réseau

Les conteneurs présents dans un même pod partagent **le même namespace réseau**. Autrement dit, les ports des conteneurs sont accessibles à tous les conteneurs présents dans le pod sans qu'aucun port ne soit exposé.

#### Stockage

Les conteneurs d'un même pod peuvent **utiliser des volumes pour partager des données** entre eux [^2].

### Exemple d'un pod multi-conteneurs

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: multiconteneur
spec:
  containers:
  - name: nginx
    image: nginx
  - name: redis
    image: redis
```

Des cas d'utilisation sont [détaillés ici](https://kubernetes.io/blog/2015/06/the-distributed-system-toolkit-patterns/).

## Pods statiques

Un **pod statique** [^3] ou **static pod** est un pod géré directement par [kubelet](../2.%20Architecture/Composants%20du%20cluster%20K8S%20-%20Control%20plane%20et%20Worker.md) et non pas par les APIs Kubernetes. Un **static pod** est obligatoirement lié à un **kubelet** d'un hôte.
Ce dernier va créer un **mirror pod** accessible depuis les **API K8S** afin que la ressource puisse être **observée et suivie** par le cluster. Ce miroir ne permet en aucun cas la gestion du **static pod**.

**Point important :** Si un **static pod** est supprimé en utilisant l'API K8S (avec la commande `kubectl` par exemple), le **mirror pod** sera supprimé puis automatiquement recréé par **kubelet**. Le **static pod** ne sera pas impacté par la suppression.

Ce type de pod est créé lorsqu'un **manifest** décrivant le pod est placé dans un dossier particulier sur le **worker node**. Le dossier configuré dans **kubelet** grâce à l'option `staticPodPath` [^4], par défaut le dossier est `/etc/kubernetes/manifests/`. Une routine va scanner régulièrement le dossier pour **créer** ou **supprimer** les ressources.

### Exemple d'un static pod

Le manifest suivant est à déposer sur le  [worker](../2.%20Architecture/Composants%20du%20cluster%20K8S%20-%20Control%20plane%20et%20Worker.md).

```yml
apiVersion: v1
kind: Pod
metadata:
  name: pod-statique
spec:
  containers:
  - name: nginx
    image: nignx:latest
```

Le manifest sera automatiquement pris en compte par **kubelet** par le biais d'une routine serveur. Pour accélérer le processus, il est possible de **redémarrer le service kubelet**.

Licence : [**`CC-BY-SA-4.0`**](https://spdx.org/licenses/CC-BY-SA-4.0.html)

[^1]: https://kubernetes.io/docs/concepts/workloads/pods/#how-pods-manage-multiple-containers
[^2]: https://kubernetes.io/docs/tasks/access-application-cluster/communicate-containers-same-pod-shared-volume/
[^3]: https://kubernetes.io/docs/tasks/configure-pod-container/static-pod/
[^4]: https://kubernetes.io/docs/reference/config-api/kubelet-config.v1beta1/
[^5]: https://kubernetes.io/docs/concepts/workloads/pods/
[^6]: https://kubernetes.io/docs/concepts/workloads/pods/pod-lifecycle/#pod-phase
