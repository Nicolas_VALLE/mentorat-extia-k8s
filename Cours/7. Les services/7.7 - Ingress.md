# Ingress

---

## Quoi ?

Ingress expose les routes HTTP et HTTPS de l'extérieur du cluster à des services au sein du cluster. Le routage du trafic est contrôlé par des règles définies sur la ressource Ingress.

Un Ingress peut être configuré pour donner aux services des URLs accessibles de l'extérieur ou encore configurer la terminaison SSL/TLS (activer le https).

Un Ingress n'expose pas de ports ni de protocoles arbitraires. Pour exposer des services autres que HTTP et HTTPS à Internet on utilise généralement un service de type Service.Type=NodePort ou Service.Type=LoadBalancer.

L'objet est Ingress n'est pas supporté nativement par Kubernetes, il est essentiel d'installer un ingress tiers pour utiliser cette fonctionnalité. Il en existe plusieurs comme Traefik, HaProxy ou encore Nginx. C'est ce dernier qui sera présenter à titre d'exemple.

## Exemple d'ingress

```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: test-ingress
  annotations:
    nginx.ingress.kubernetes.io/rewrite-target: /
spec:
  rules:
  - http:
      paths:
      - path: /testpath
        pathType: Prefix
        backend:
          service:
            name: test
            port:
              number: 80
```

L'ingress a aussi besoin d'un service, dans le cadre de l'exemple on reprendra le même service basique que le chapitre précédent.

```yaml
apiVersion: v1
kind: Service
metadata:
  name: test
spec:
  selector:
    app.kubernetes.io/name: MyApp
  ports:
    - protocol: TCP
      port: 80
      targetPort: 9376
```

Comme pour toutes les autres ressources Kubernetes, un Ingress (une entrée) a besoin des champs apiVersion, kind et metadata.

Ingress utilise fréquemment des annotations pour configurer certaines options. Ces options diffères en fonction du contrôleur Ingress utilisé. Un exemple est l'annotation rewrite-target.

Pour la partie rules, on retrouve les options suivantes:
- une liste de chemins (dans l'exemple, /testpath), chacun étant associé à un backend associé défini par un serviceName et servicePort
- Un backend qui est une combinaison de noms de services et de ports.

Dans notre cas lorsque l'utilisateur renseignera une url du type `mon_url/testpath` il sera redirigé vers une url du type `mon_url` grâce à `nginx.ingress.kubernetes.io/rewrite-target: /`.

Cette page web, à laquelle il accède par le port 80, lui affichera ce qu'il y a d'hébergé sur le pod qui utilise notre service `my-service` sur le port 9376.