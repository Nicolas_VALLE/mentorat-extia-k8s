# API Server

---

## Quoi ?

Orchestrateur de containeur open-source.
K8S est une abréviation de kubernetes. Il ajoute une couche d'abstraction pour faire fonctionner les conteneurs sur un cluster de serveur. Peu importe sur quel serveur les conteneurs fonctionnent tant qu'ils dans un état déclaré.

## Fonctionnalités

Il permet l'auto-healing d'une application si elle ne répond pas correctement et l'auto-scalabilité. K8S est construit de façon à proposer du **tout API** [^1] pour ces différents modules. Cela permet d'incorporer beaucoup de fonctionnalités supplémentaires à un cluster standard.

Licence : [**`CC-BY-SA-4.0`**](https://spdx.org/licenses/CC-BY-SA-4.0.html)

[^1]: https://kubernetes.io/docs/concepts/overview/kubernetes-api/
