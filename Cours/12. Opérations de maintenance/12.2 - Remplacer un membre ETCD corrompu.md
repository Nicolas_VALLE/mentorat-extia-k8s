# Remplacer un membre ETCD corrompu

## Date : 14 décembre 2022

---

## Quoi ?

Il peut arriver qu'un membre du cluster **ETCD** soit corrompu. Dans ce cas, un message d'erreur s'affiche dans les **logs** d'ETCD, quelque soit son [architecture d'installation](../2.%20Architecture/2.3%20-%20ETCD.md).

Le log d'erreur peut ressembler à ça :

```log
raft2022/12/14 19:34:26 tocommit(83524555) is out of range [lastIndex(83524552)]. Was the raft log corrupted, truncated, or lost?
panic: tocommit(83524555) is out of range [lastIndex(83524552)]. Was the raft log corrupted, truncated, or lost?
```

La réparation d'un nœud se réalise grâce à la commande `etcdctl`, elle peut être installée grâce à la commande `apt install etcd-client`. Cette commande est également utilisée pour [sauvegarder ETCD](12.3%20-%20Sauvegarde%20ETCD.md).

## Comment réparer l'erreur ?

Dans un premier temps, il est nécessaire de réaliser une sauvegarde des données du cluster ETCD :

```bash
ETCDCTL=$(find / -name etcdctl -type f | head -n1)
ETCDCTL_API=3 \
  ${ETCDCTL} \
  --cert=/etc/kubernetes/pki/etcd/server.crt \
  --key=/etc/kubernetes/pki/etcd/server.key \
  --cacert=/etc/kubernetes/pki/etcd/ca.crt \
  --endpoints=https://127.0.0.1:2379 \
  snapshot save ~/$(date -I)-etcd-snapshot.db
```

### Sur le nœud corrompu

Les commandes suivantes sont à réaliser sur le noeud corrompu.

1. Supprimer la configuration d'ETCD :

    ```bash
    mv /etc/kubernetes/manifests/etcd.yaml /tmp
    mv /etc/lib/etcd/member /tmp
    ```

2. Remettre à zéro la configuration d'ETCD :

    ```bash
    kubeadm reset phase remove-etcd-member
    ```

    On peut vérifier que le noeud est bien sorti cluster en tapant la commande suivante sur un **noeud sein du cluster** :

    ```bash
    ETCDCTL=$(find / -name etcdctl -type f | head -n1)
    ETCDCTL_API=3 \
    ${ETCDCTL} \
    --cert=/etc/kubernetes/pki/etcd/server.crt \
    --key=/etc/kubernetes/pki/etcd/server.key \
    --cacert=/etc/kubernetes/pki/etcd/ca.crt \
    --endpoints=https://127.0.0.1:2379 \
    endpoint status --cluster -w table
    ```

3. Joindre de nouveau le cluster :

    ```bash
    kubeadm join phase control-plane-join etcd --control-plane
    ```

4. Redémarrer le serveur afin que **kube-apiserver** puisse se connecter correment au cluster:

    ```bash
    reboot
    ```

5. Vérifier que le noeud remonte correctement dans le cluster ETCD :

    ```bash
    ETCDCTL=$(find / -name etcdctl -type f | head -n1)
    ETCDCTL_API=3 \
    ${ETCDCTL} \
    --cert=/etc/kubernetes/pki/etcd/server.crt \
    --key=/etc/kubernetes/pki/etcd/server.key \
    --cacert=/etc/kubernetes/pki/etcd/ca.crt \
    --endpoints=https://127.0.0.1:2379 \
    endpoint status --cluster -w table
    ```

Licence : [**`CC-BY-SA-4.0`**](https://spdx.org/licenses/CC-BY-SA-4.0.html)
