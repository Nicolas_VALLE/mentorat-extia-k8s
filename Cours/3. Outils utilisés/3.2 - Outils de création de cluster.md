# Outils de création de cluster

---

## Quoi ?

La liste d'outil suivante permet de créer un cluster Kubernetes. **Kubeadm** est le seul outil de la liste à pouvoir **créer un cluster en haute disponibilité**.

Les deux autres outils sont à utiliser en local sur un PC pour des fins de test.

## kubeadm

Kubeadm [^1] [^2] [^3] est un outil permettant de simplifier la création du cluster K8S. Il permet notamment de paramétrer le **[control plane](../2.%20Architecture/2.1%20-%20Composants%20du%20cluster%20K8S%20-%20Control%20plane%20et%20Worker.md#Les%20composants%20du%20Controller)** et d'ajouter de nouveau **[worker nodes](../2.%20Architecture/2.1%20-%20Composants%20du%20cluster%20K8S%20-%20Control%20plane%20et%20Worker.md#Les%20composants%20du%20Worker)** au cluster

## minikube

Minikube permet de créer localement une instance de K8S fonctionnant sur un seul nœud. Cela permet de réaliser et de simplifier les tests et les développements.

## kinds

Licence : [**`CC-BY-SA-4.0`**](https://spdx.org/licenses/CC-BY-SA-4.0.html)

[^1]: https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/create-cluster-kubeadm/
[^2]: https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/
[^3]: https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/high-availability/
