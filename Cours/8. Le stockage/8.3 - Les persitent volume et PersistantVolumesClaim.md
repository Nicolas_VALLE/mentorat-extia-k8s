# Les persitent volume et PersistantVolumesClaim

---

## Quoi ?

La différence fondamentale entre les `Persistent Volumes` et les volumes natifs de Docker (`emptyDir`,`hostPath` etc...), est que ces volumes ne sont pas physiquement stockés sur les nodes, mais la plupart du temps sur un service de stockage physiquement externe au cluster Kubernetes, accessible par le réseau.

Un `PersistentVolumeClaim` (PVC) est une demande de stockage par un utilisateur. Il est similaire à un Pod. Les pods consomment des ressources de noeud et les PVC consomment des ressources PV. Les pods peuvent demander des niveaux spécifiques de ressources (CPU et mémoire). Les PVC peuvent demander une taille et des modes d'accès spécifiques (par exemple, ils peuvent être montés une fois en lecture/écriture ou plusieurs fois en lecture seule).

Pour être utilisable, il y a deux nouvelles ressources API: `PersistentVolume` et `PersistentVolumeClaim`.

Il y a une autre notion importante dans le cadre des `PersistentVolume` et `PersistentVolumeClaim` c'est la `StorageClass`.
Pour l'instant nous ne rentrerons pas dans les détail mais c'est ce qui permet de mapper et `PersistentVolume` et `PersistentVolumeClaim`, dans le cadre ou il n'y a qu'un `PersistentVolume` ce n'est pas necessaire d'en spécifier car `StorageClass` par défaut suffira.

A noter aussi qu'il existe beaucoup d'option de configuration pour les `PersistentVolume` et les `PersistentVolumeClaim` pour éviter de trop complexifié la chose on restera sur une configuration assez basique.

### Exemple


```yaml
kind: PersistentVolume
apiVersion: v1
metadata:
   name: access-log-pv
spec:
  # La storageClass utilisée par défaut n'a pas besoin d'être renseigné mais pourrait être appelée comme suit: storageClassName: ""
  # Pour utiliser une storageClass particulière il faut utiliser la ligne suivante:
  # storageClassName: "sc1"
  capacity:
    storage: 1Gi
  accessModes:
    - ReadWriteOnce
  hostPath:
    path: /var/log/syslog
    type: DirectoryOrCreate

---

kind: PersistentVolumeClaim
apiVersion: v1
metadata:
   name: access-log-pvc
spec:
   # La storageClass utilisée par défaut n'a pas besoin d'être renseigné mais pourrait être appelée comme suit: storageClassName: ""
   # Pour utiliser une storageClass particulière il faut utiliser la ligne suivante:
   # storageClassName: "sc1"
   accessModes:
      - ReadWriteOnce
   resources:
      requests:
        storage: 500Mi

---

kind: Pod
apiVersion: v1
metadata:
  name: test
spec:
  volumes:
    - name: access-log-pvc-vol
      persistentVolumeClaim:
        claimName: access-log-pvc
  containers:
    - name: proxy
      image: nginx
      ports:
        - containerPort: 80
          name: "http-server"
      volumeMounts:
        - mountPath: "/nginx/"
          name: access-log-pvc-vol
```

Ici on créer le `PersistentVolume` `access-log-pv` de 1Go, puis dedans on créer le `PersistentVolumeClaim` `access-log-pvc` de 500Mo.
Ensuite dans notre pod on vient créer le volume `access-log-pvc-vol` qui utilise le `PersistentVolumeClaim` créé précédemment.