# Statefull vs Stateless

---

## Quoi ?

Les termes "stateful" (avec état) et "stateless" (sans état) sont couramment utilisés pour décrire les applications ou les workloads déployé dans un cluster Kubernetes.

### Stateless

Une application Stateless est conçue de manière à ce qu'elle ne stocke pas d'informations ou d'état significatif sur le serveur où elle s'exécute. Cela signifie que chaque requête ou transaction est traitée de manière indépendante, sans que l'application ait besoin de se souvenir de l'état précédent.

Les applications Stateless sont souvent hautement évolutives, car elles peuvent être facilement déployées sur de multiples instances (pods) sans se soucier de la cohérence des données ou de la gestion de l'état. Elles sont idéales pour les charges de travail où les requêtes sont indépendantes les unes des autres, comme les serveurs web ou les microservices de calcul intensif.

Pour les applications Stateless, Kubernetes est généralement utilisé pour déployer des conteneurs (pods) qui peuvent être facilement mis à l'échelle horizontalement pour gérer des charges de travail variables. Ces applications tirent pleinement parti de l'orchestration et de la gestion automatisée de Kubernetes.

### Stateful

Une application Stateful, en revanche, stocke des données ou de l'état qui sont significatifs et persistents entre les requêtes ou les transactions. Ces données peuvent inclure des bases de données, des caches, des systèmes de fichiers, etc.

Les applications Stateful sont plus complexes à gérer, car elles nécessitent une prise en charge de la persistance des données et de la cohérence de l'état. Elles sont généralement moins évolutives que les applications Stateless car la gestion de l'état peut poser des défis en matière de réplication et de distribution.

Les applications Stateful dans Kubernetes sont généralement déployées à l'aide de StatefulSets, un contrôleur spécifique qui permet de gérer des pods avec un état persistant. Les StatefulSets attribuent des noms persistants aux pods et gèrent l'ordre de démarrage, ce qui est essentiel pour les applications nécessitant une cohérence d'état.

#### StatefulSets

Les StatefulSets sont utilisés lorsque vous avez besoin de garantir un ordre d'initialisation spécifique, une identité persistante pour chaque instance de l'application, et la stabilité de l'état entre les redéploiements.

```yaml
apiVersion: v1
kind: Service
metadata:
  name: nginx
  labels:
    app: nginx
spec:
  ports:
  - port: 80
    name: web # Nom du port
  clusterIP: None
  selector:
    app: nginx # Service utilisable par les pods ayant un champ template: metadata: labels égale à "app: nginx"
---
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: web
spec:
  selector:
    matchLabels:
      app: nginx # Selector qui détermine quels pods appartiennent au StatefulSet, doir correspondre aux champ template: metadata: labels
  serviceName: "nginx"
  replicas: 3
  template:
    metadata:
      labels:
        app: nginx # Match le selector défini pour identifier les pods du StatefulSet
    spec:
      terminationGracePeriodSeconds: 10
      containers:
      - name: nginx
        image: registry.k8s.io/nginx-slim:0.8
        ports:
        - containerPort: 80
          name: web # Utilisation du nom de port de service
        volumeMounts:
        - name: www
          mountPath: /usr/share/nginx/html
  volumeClaimTemplates:
  - metadata:
      name: www
    spec:
      accessModes: [ "ReadWriteOnce" ]
      resources:
        requests:
          storage: 1Gi
```

Ici on défini:

* Un objet `Service` nommé nginx est créé, comme indiqué par le champ `metadata: name`. L'objet `Service` cible une application appelée nginx, définie par les champs `labels: app: nginx` et `selector: app: nginx`. Le `Service` exploite le port 80 et le nomme web. Ce service contrôle le domaine de réseau et achemine le trafic Internet vers l'application en conteneur déployée par l'objet `StatefulSet`.
* Un `StatefulSet` nommé web est créé avec trois pods répliqués (`replicas: 3`).
* Le modèle de pod (`spec: template`) indique que ses pods sont associés au libellé `app: nginx`.
* La spécification de pod (`template: spec`) indique que les pods de l'objet `StatefulSet` exécutent un conteneur, nginx, qui exécute l'image nginx-slim dans sa version 0.8.
* La spécification de pod utilise le port web ouvert par le `Service`.
* `template: spec: volumeMounts` spécifie un chemin `mountPath`, nommé www. Le `mountPath`st le chemin, au sein du conteneur, sur lequel un volume de stockage doit être installé.

### En résumé

La principale différence entre Stateful et Stateless dans Kubernetes réside dans la manière dont les données ou l'état sont gérés. Stateless signifie que chaque requête est indépendante et que l'application n'a pas besoin de se souvenir de l'état précédent, tandis que Stateful implique que l'application conserve un état significatif et persistant entre les requêtes ou les transactions, ce qui peut rendre la gestion plus complexe.