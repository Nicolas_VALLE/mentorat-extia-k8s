# Les volumes

---

## Quoi ?

Les fichiers dans un conteneur sont éphémères par défaut, cela représente deux problèmes.
* Lorsqu'un conteneur plante, il va redémarrer mais les fichiers seront perdus.
* Lorsque plusieurs conteneurs s'exécutent ensemble dans un Pod, il est souvent nécessaire de partager des fichiers entre ces conteneurs.

La notion de volume permet de résoudre ces deux problèmes.
Il existe beacoup de type de volume différent(la liste complète [ici](https://kubernetes.io/fr/docs/concepts/storage/volumes/#types-de-volumes))
Dans ce chapitre nous abordrons les deux plus communs `emptyDir`et `hostPath`
A noté aussi que les `secret` et les `configmap` vu dans un [chapitre précedent](../5.%20Les%20pods/5.8%20-%20ConfigMap%20et%20Secret.md) peuvent être utilisé comme des volumes.

### emptyDir

Un volume `emptyDir` est initialement vide. Les conteneurs dans le Pod peuvent tous lire et écrire les mêmes fichiers dans le volume emptyDir, bien que ce volume puisse être monté sur le même ou différents chemins dans chaque conteneur.

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: production
spec:
  containers:
  - name: container1
    image: image1
    volumeMounts:
    - name: storage
      mountPath: /vol/data
  - name: container2
    image: image2
    volumeMounts:
    - name: storage
      mountPath: /store/data
  volumes:
  - name: storage
    emptyDir: {}
```

Ici on créer un `emptyDir` nommé `storage`, sur le `container1` il est monté dans `/vol/data` et sur le `container2` il est monté dans `/store/data`

### hostPath

Un volume `hostPath` monte un fichier ou un dossier depuis le système de fichiers du nœud hôte à l'intérieur d'un Pod.
Il existe plusieurs options utilisable par un `hostPath`

* DirectoryOrCreate: Si rien n'existe au chemin fourni, un dossier vide y sera créé au besoin avec les permissions définies à 0755.
* Directory: Un dossier doit exister au chemin fourni.
* FileOrCreate: Si rien n'existe au chemin fourni, un fichier vide y sera créé au besoin avec les permissions définies à 0644.
* File: Un fichier doit exister au chemin fourni.
* Socket: Un socket UNIX doit exister au chemin fourni.
* CharDevice: Un périphérique en mode caractère doit exister au chemin fourni.
* BlockDevice: Un périphérique en mode bloc doit exister au chemin fourni.

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: test-webserver
spec:
  containers:
  - name: test-webserver
    image: registry.k8s.io/test-webserver:latest
    volumeMounts:
    - mountPath: /var/local/aaa
      name: mydir
    - mountPath: /var/local/aaa/1.txt
      name: myfile
  volumes:
  - name: mydir
    hostPath:
      path: /var/local/aaa
      type: DirectoryOrCreate
  - name: myfile
    hostPath:
      path: /var/local/aaa/1.txt
      type: FileOrCreate
```