# Le DaemonSet

## Date : 09 décembre 2022

---

## Quoi ?

Un **DaemonSet** [^1] est un type de déploiement permettant de déployer un pod sur chaque **worker node** du cluster. Si un nouveau [worker](../2.%20Architecture/2.1%20-%20Composants%20du%20cluster%20K8S%20-%20Control%20plane%20et%20Worker.md#Les%20composants%20du%20Worker) est ajouté au cluster, le pod du **DaemonSet** sera automatiquement créé dessus.

Les **DaemonSet** suivent les mêmes règles de déploiement qu'un pod standard. Les règles liées aux **labels**, aux **taints** ou aux **tolerations** sont suivies.

## Exemples

```yaml
apiVersion: apps/v1
kind: DaemonSet
metadata:
  name: mon-daemonset
spec:
  selector:
    matchLabels:
      app: mon-daemonset
  template:
    metadata:
      labels:
        app: mon-daemonset
    spec:
      containers:
      - name: nginx
        image: nginx
```

Licence : [**`CC-BY-SA-4.0`**](https://spdx.org/licenses/CC-BY-SA-4.0.html)

[^1]: https://kubernetes.io/docs/concepts/workloads/controllers/daemonset/
