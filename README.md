# Mentorat Kubernetes

Ce dépôt à pour but de partager des connaissances autours de Kubernetes et de former toutes personnes sur cet outil.

Deux dossiers sont présents à la racine :

- le dossier **Cours** décris certains fonctionnements de kubernetes et décris également son architecture.
- le dossier **TP** est une liste d'exercices venant en complément de la partie Cours.

## Plan

1. Concept général
    1. Conteneur vs vm (rappel docker)
    2. Avantage ops dev et entreprise
    3. [Impératif et déclaratif](Cours/1.%20Concepts%20généraux/1.3%20-%20Impératif%20et%20déclaratif.md)
2. Architecture
    1. [Composants du cluster K8S - Control plane et Worker](Cours/2.%20Architecture/2.1%20-%20Composants%20du%20cluster%20K8S%20-%20Control%20plane%20et%20Worker.md)
    2. Workflow de déploiement
    3. [ETCD](Cours/2.%20Architecture/2.3%20-%20ETCD.md)
    4. [Installation d'un cluster K8S](Cours/2.%20Architecture/2.4%20-%20Installation%20d'un%20cluster%20K8S.md)
3. Outils utilisés
    1. [Outils d'administration du cluster](Cours/3.%20Outils%20utilisés/3.1%20-%20Outils%20d'administration%20du%20cluster.md) **A compléter**
    2. [Outils de création de cluster](Cours/3.%20Outils%20utilisés/3.2%20-%20Outils%20de%20création%20de%20cluster.md) **A compléter**
    3. focus sur minikube
    4. [Kubectl](Cours/3.%20Outils%20utilisés/3.4%20-%20Kubectl.md)
4. Les bases de K8S
    1. Concept des applications (K8S vs standard) : bonne pratique
    2. Les bases des ressources (api version, kind, metadata)
    3. [API Server](Cours/4.%20Les%20bases%20de%20K8S/4.3%20-%20API%20Server.md) **A modifier/compléter**
5. Les pods **TP**
    1. [Pods simples, multi-conteneurs, pods statiques](Cours/5.%20Les%20pods/5.1%20-%20Pods%20simples,%20multi-conteneurs,%20pods%20statiques.md)
    2. [Namespaces et contexte](Cours/5.%20Les%20pods/5.2%20-%20Namespaces%20et%20contexte.md)
    3. [Restart Policy](Cours/5.%20Les%20pods/5.3%20-%20Restart%20Policy.md)
    4. [Probes](Cours/5.%20Les%20pods/5.4%20-%20Probes.md)
    5. [Limit et Request](Cours/5.%20Les%20pods/5.5%20-%20Limit%20et%20Request.md)
    6. [Init-containers](Cours/5.%20Les%20pods/5.6%20-%20Init-containers.md)
    7. forward de pod/exposition de port
    8. [ConfigMap et Secret](Cours/5.%20Les%20pods/5.8%20-%20ConfigMap%20et%20Secret.md)
    9. [NodeSelector et NodeName](Cours/5.%20Les%20pods/5.9%20-%20NodeSelector%20et%20NodeName.md)**Vérifier si la partie kube-scheduler a bien été traitée**
6. Les déploiements **TP**
    1. [Principes de bases](Cours/6.%20Les%20déploiements/6.1%20-%20Principes%20de%20bases.md)
    2. [Scaling](Cours/6.%20Les%20déploiements/6.2%20-%20Scaling.md)
    3. Différents types de déploiement (rolling update, canary, blue/green)
    4. [DaemonSet](Cours/6.%20Les%20déploiements/6.4%20-%20DaemonSet.md)
7. Les services **TP**
    1. [Principe des différents services](Cours/7.%20Les%20services/7.1%20-%20Principe%20des%20diff%C3%A9rents%20services.md)
    2. [DNS pour les services et les pods](Cours/7.%20Les%20services/7.2%20-%20Principe%20des%20diff%C3%A9rents%20services.md)
    3. [ClusterIP](Cours/7.%20Les%20services/7.3%20-%20Clusterip.md)
    4. [Nodeport](Cours/7.%20Les%20services/7.4%20-%20Nodeport.md)
    5. [LoadBalancer](Cours/7.%20Les%20services/7.5%20-%20Loadbalancer.md)
    6. [ExternalName](Cours/7.%20Les%20services/7.6%20-%20ExternalName.md)
    7. [Ingress](Cours/7.%20Les%20services/7.7%20-%20Ingress.md)
8. Le stockage **TP**
    1. [Les principes](cours/8.%20Le%20stockage/8.1%20-%20Les%20principes.md)
    2. [Les volumes](cours/8.%20Le%20stockage/8.2%20-%20Les%20volumes.md)
    3. [Les persitent volume et PersistantVolumesClaim](cours/8.%20Le%20stockage/8.3%20-%20Les%20persitent%20volume%20et%20PersistantVolumesClaim.md)
    4. [Stateful vs Stateless](cours/8.%20Le%20stockage/8.4%20-%20Statefull%20vs%20Stateless.md)
    5. StorageClass
9. Le réseau **TP**
    1. L'architecture réseau et les plugins CNI
    2. Les polices réseaux
10. La gestion des droits **TP**
    1. [RBAC](Cours/10.%20La%20gestion%20des%20droits/10.1%20-%20RBAC.md)
    2. [ServiceAccount](Cours/10.%20La%20gestion%20des%20droits/10.2%20-%20ServiceAccount.md)
11. Notions avancées
    1. [HELM](Cours/11.%20Notions%20avancées/11.1%20-%20HELM.md)
    2. [Ressources et métriques](Cours/11.%20Notions%20avancées/11.2%20-%20Ressources%20et%20métriques.md)
12. Opérations de maintenance
    1. [Ajout d'un utilisateur](Cours/12.%20Opérations%20de%20maintenance/12.1%20-%20Ajout%20d'un%20utilisateur.md)
    2. [Remplacer un membre ETCD corrompu](Cours/12.%20Opérations%20de%20maintenance/12.2%20-%20Remplacer%20un%20membre%20ETCD%20corrompu.md)
    3. [Sauvegarde ETCD](Cours/12.%20Opérations%20de%20maintenance/12.3%20-%20Sauvegarde%20ETCD.md)
    4. [Maintenance d'un noeud de K8S](Cours/12.%20Opérations%20de%20maintenance/12.4%20-%20Maintenance%20d'un%20noeud%20de%20K8S.md)
    5. [Mise à jour de K8S](Cours/12.%20Opérations%20de%20maintenance/12.5%20-%20Mise%20à%20jour%20de%20K8S.md)

## Aides

Un [cheat sheet](https://kubernetes.io/docs/reference/kubectl/cheatsheet/) est disponible dans la doc de K8S.

## Licence

Le projet est distribué sous la licence **Creative Commons Atribution-ShareAlike 4.0 International**. Il peut donc être partagé et adapté librement par tous sous certaines conditions :

**Attribution** — Vous devez créditer l'Œuvre, intégrer un lien vers la licence et indiquer si des modifications ont été effectuées à l'Oeuvre. Vous devez indiquer ces informations par tous les moyens raisonnables, sans toutefois suggérer que l'Offrant vous soutient ou soutient la façon dont vous avez utilisé son Oeuvre.

**Partage dans les Mêmes Conditions** — Dans le cas où vous effectuez un remix, que vous transformez, ou créez à partir du matériel composant l'Oeuvre originale, vous devez diffuser l'Oeuvre modifiée dans les même conditions, c'est à dire avec la même licence avec laquelle l'Oeuvre originale a été diffusée.

Pour plus d'informations : visiter le site [creativecommon.org](https://creativecommons.org/licenses/by-sa/4.0/deed).
